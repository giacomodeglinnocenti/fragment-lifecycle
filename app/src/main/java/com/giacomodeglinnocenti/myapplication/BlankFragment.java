package com.giacomodeglinnocenti.myapplication;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class BlankFragment extends Fragment {

    private ListView listView;
    private ArrayAdapter<String> adapter;
    private Resources res;
    private String[] plans;

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toast.makeText(getContext(),"Fragment onCreateView called",Toast.LENGTH_SHORT).show();
        System.out.println("Fragment onCreateView called");
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toast.makeText(getContext(),"Fragment OnActivityCreated called",Toast.LENGTH_SHORT).show();
        System.out.println("Fragment OnActivityCreated called");

        listView = (ListView) getActivity().findViewById(R.id.fragment_list);
        res = getResources();
        plans = res.getStringArray(R.array.plans);
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                android.R.id.text1, plans);
        listView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
